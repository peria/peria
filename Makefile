build-local:
	sh scripts/build-cli.sh local
# cli
login: build-local
	./peria login

hello: build-local
	cd examples/hello && ../../peria publish

hello2: build-local
	cd examples/hello2 && ../../peria publish

publish-big: build-local
	cd examples/hello_big && openssl rand -base64 -out delme 10000000 && ../../peria publish

release:
	sh scripts/build-cli.sh all
	rsync -avp --rsync-path="sudo -u peria rsync" dist/bin debian@peria.tiagosimao.com:/home/peria/web/

# api
serve:
	tilt up

#deploy-server:
#	sh scripts/build.sh server
#	rsync -avp --rsync-path="sudo rsync" dist/bin/peria-linux-amd64-latest debian@peria.tiagosimao.com:/usr/local/bin/peria

# web 
deploy-web:
	mkdir -p dist/web/
	cp -R website/* dist/web/
	rsync -avp --rsync-path="sudo -u peria rsync" dist/web debian@peria.tiagosimao.com:/home/peria/

#proto
proto:
	protoc --go_out=./pkg --go_opt=paths=source_relative --go-grpc_out=./pkg --go-grpc_opt=paths=source_relative api/peria.proto

# infra
kind:
	kind delete cluster --name peria
	kind create cluster --config .kind/kind.yaml
# TODO, get Caddy cert and add it to the client config
# echo quit | openssl s_client -showcerts -servername "api.peria.pt" -connect "api.peria.pt":1443 > cacert.pem
#kubectl apply -f https://projectcontour.io/quickstart/contour.yaml
#kubectl patch daemonsets -n projectcontour envoy --patch-file .kind/contour.kind.json
#kubectl apply -f https://projectcontour.io/quickstart/contour-gateway-provisioner.yaml
#kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml
#kubectl wait --namespace metallb-system --for=condition=ready pod --selector=app=metallb --timeout=90s
#kubectl apply -f .kind/metallb.yaml
#TODO: render ./kind/metallb.yaml with the contents of
# docker network inspect -f '{{.IPAM.Config}}' kind

cloud-init:
	cat .env scripts/init.sh | ssh debian@peria.tiagosimao.com 'bash -s'
