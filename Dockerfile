FROM golang:1.20.2-alpine3.17 as build
ENV CGO_ENABLED=0
WORKDIR /src
RUN printf "app:x:1000:1000:Linux User,,,:/_:/bin/ash" > passwd
COPY go.* .
RUN go mod download
COPY . .
RUN go build -ldflags "-w -s" -o app codeberg.org/peria/cli/cmd/api

FROM scratch
COPY --from=build /src/passwd /etc/passwd
USER app
COPY --from=build /src/app .
ENTRYPOINT ["/app"]
