#!/bin/sh
REPO="https://peria.tiagosimao.com/bin"
OS=""
ARCH=""
SUFFIX=""
VERSION="latest"

fatal() {
    echo "$1" >&2
    exit 1
}

ensure() {
    if ! "$@"; then fatal "command failed: $*"; fi
}

check_cmd() {
    command -v "$1" > /dev/null 2>&1
}

need_cmd() {
    if ! check_cmd "$1"; then
        fatal "need '$1' (command not found)"
    fi
}

detect_architecture() {
    local _os _arch
    _os="$(uname -s)"
    _arch="$(uname -m)"

    case "$_os" in
        Linux)
            _os=linux
            ;;
        Darwin)
            _os=darwin
            ;;
        *)
            fatal "unsupported OS type: $_os"
            ;;
    esac

    case "$_arch" in
        aarch64 | arm64)
            _arch=arm64
            ;;
        x86_64 | x86-64 | x64 | amd64)
            _arch=amd64
            ;;
        *)
            fatal "unsupported CPU type: $_arch"
    esac

    OS="${_os}"
    ARCH="${_arch}"
}

downloader() {
    local _dld
    local _ciphersuites
    local _err
    local _status
    local _retry
    if check_cmd curl; then
        _dld=curl
    elif check_cmd wget; then
        _dld=wget
    else
        _dld='curl or wget'
    fi

    if [ "$1" = --check ]; then
        need_cmd "$_dld"
    elif [ "$_dld" = curl ]; then
        _err=$(curl $_retry --proto '=https' --tlsv1.2 --silent --show-error --fail --location "$1" --output "$2" 2>&1)
        _status=$?
        if [ -n "$_err" ]; then
            echo "$_err" >&2
            if echo "$_err" | grep -q 404$; then
                err "installer for platform '$3' not found, this may be unsupported"
            fi
        fi
        return $_status
    elif [ "$_dld" = wget ]; then
        if [ "$(wget -V 2>&1|head -2|tail -1|cut -f1 -d" ")" = "BusyBox" ]; then
            _err=$(wget "$1" -O "$2" 2>&1)
            _status=$?
        else
            _err=$(wget --https-only --secure-protocol=TLSv1_2 "$1" -O "$2" 2>&1)
            _status=$?
        fi
        if [ -n "$_err" ]; then
            echo "$_err" >&2
            if echo "$_err" | grep -q ' 404 Not Found$'; then
                err "installer for platform '$3' not found, this may be unsupported"
            fi
        fi
        return $_status
    fi
}



main() {
    downloader --check
    need_cmd uname
    need_cmd mktemp
    need_cmd chmod
    need_cmd mkdir
    need_cmd rm
    need_cmd rmdir

    detect_architecture

    local _filename="peria-${OS}-${ARCH}-${VERSION}${SUFFIX}"
    local _url="${REPO}/${_filename}"
    local _dir
    _dir="$(ensure mktemp -d)"
    local _file="${_dir}/${_filename}"

    local _ansi_escapes_are_valid=false
    if [ -t 2 ]; then
        if [ "${TERM+set}" = 'set' ]; then
            case "$TERM" in
                xterm*|rxvt*|urxvt*|linux*|vt*)
                    _ansi_escapes_are_valid=true
                ;;
            esac
        fi
    fi

    if $_ansi_escapes_are_valid; then
        printf "\33[1minfo:\33[0m downloading installer\n" 1>&2
    else
        printf '%s\n' 'info: downloading installer' 1>&2
    fi

    ensure mkdir -p "$_dir"
    ensure downloader "$_url" "$_file" "$_arch"
    ensure chmod u+x "$_file"

    if mv "$_file" /usr/local/bin/peria ; then
        echo "successfully installed peria, try it out running:"
        printf "\tperia -h\n"
    else
        echo "error creating /usr/local/bin/peria"
    fi

    rm "$_file" 1> /dev/null 2> /dev/null
    rmdir "$_dir" 1> /dev/null 2> /dev/null
}

main