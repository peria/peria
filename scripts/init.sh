#!/bin/bash
set -euxo pipefail

sudo -i PERIA_ADMIN_FQDN=$PERIA_ADMIN_FQDN PERIA_SERVING_FQDN=$PERIA_SERVING_FQDN PERIA_GANDI_API_TOKEN=$PERIA_GANDI_API_TOKEN
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export DEBIAN_FRONTEND="noninteractive"

apt-get install -y debian-keyring debian-archive-keyring apt-transport-https

apt-get -y install unattended-upgrades rsync podman ufw

echo "Firewall"
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh
ufw allow http
ufw allow https
ufw --force enable

echo "Postgres"
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
apt-get -y install postgresql

echo "Hydra"
if [ ! -d /home/hydra ]; then
    adduser --system --quiet hydra
fi
if [ ! -f /home/hydra/pg_password ]; then
    printf $(export LC_CTYPE=C; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) > /home/hydra/pg_password
    sudo -Hiu postgres psql -c "CREATE USER hydra PASSWORD '$(cat /home/hydra/pg_password)';"
    sudo -Hiu postgres createdb hydra -O hydra
fi
if [ ! -f /home/hydra/secret ]; then
    printf $(export LC_CTYPE=C; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) > /home/hydra/secret
fi
if [ ! -f /usr/local/bin/hydra ]; then
    bash <(curl https://raw.githubusercontent.com/ory/meta/master/install.sh) -d -b . hydra v2.0.3
    mv ./hydra /usr/local/bin/hydra
    hydra migrate sql -y postgres://hydra:$(cat /home/hydra/pg_password)@127.0.0.1:5432/hydra?sslmode=disable&search_path=public
fi

cat > /etc/systemd/system/hydra.service <<EOL
[Unit]
Description=Hydra
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=hydra
Environment=SERVE_ADMIN_HOST=127.0.0.1
Environment=SERVE_PUBLIC_HOST=127.0.0.1
Environment=DSN=postgres://hydra:$(cat /home/hydra/pg_password)@127.0.0.1:5432/hydra?sslmode=disable&max_conns=20&max_idle_conns=4&search_path=public
Environment=SECRETS_SYSTEM=$(cat /home/hydra/secret)
Environment=URLS_SELF_ISSUER=https://auth.$PERIA_ADMIN_FQDN/
Environment=STRATEGIES_ACCESS_TOKEN=jwt
ExecStart=/usr/local/bin/hydra serve all

[Install]
WantedBy=multi-user.target
EOL

systemctl enable hydra.service
systemctl restart hydra.service


echo "Peria"
if [ ! -d /home/peria ]; then
    adduser --system --quiet peria
fi
if [ ! -f /home/peria/pg_password ]; then
    printf $(export LC_CTYPE=C; cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) > /home/peria/pg_password
    sudo -Hiu postgres psql -c "CREATE USER peria PASSWORD '$(cat /home/peria/pg_password)';"
    sudo -Hiu postgres createdb peria -O peria
fi
cat > /etc/sudoers.d/10-peria-user <<EOL
peria ALL=(ALL) NOPASSWD:ALL
EOL
chmod 0440 /etc/sudoers.d/10-peria-user

cat > /etc/systemd/system/peria.service <<EOL
[Unit]
Description=Peria API
Wants=network.target

[Service]
ExecStart=/usr/local/bin/peria --server
Restart=always
RestartSec=1
User=peria
Environment=PERIA_ADMIN_FQDN=$PERIA_ADMIN_FQDN
Environment=PERIA_SERVING_FQDN=$PERIA_SERVING_FQDN
Environment=PERIA_OIDC_URL=https://auth.$PERIA_ADMIN_FQDN
Environment=PERIA_CADDY_URL=http://localhost:2019
Environment=PERIA_HYDRA_ADMIN_URL=http://localhost:4445
Environment=PERIA_GANDI_API_TOKEN=$PERIA_GANDI_API_TOKEN
Environment=PERIA_DSN=postgres://peria:$(cat /home/peria/pg_password)@127.0.0.1:5432/peria?sslmode=disable&search_path=public

[Install]
WantedBy=multi-user.target
EOL

cat > /etc/systemd/system/deploy.service <<EOL
[Unit]
Description=Peria restarter
After=network.target

[Service]
Type=oneshot
ExecStart=/usr/bin/systemctl restart peria.service

[Install]
WantedBy=multi-user.target
EOL

cat > /etc/systemd/system/deploy.path <<EOL
[Path]
Unit=deploy.service
PathChanged=/usr/local/bin/peria 

[Install]
WantedBy=multi-user.target
EOL

systemctl enable deploy.path
systemctl enable deploy.service
systemctl start deploy.path
systemctl start deploy.service
systemctl enable --now peria.service

echo "Caddy"
if [ ! -f /usr/local/bin/caddy ]; then
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | gpg --batch --yes --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg
    curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | tee /etc/apt/sources.list.d/caddy-stable.list
    apt-get -y update
    apt-get -y install caddy

    mkdir /tmp/xcaddy
    cat > /tmp/xcaddy/Dockerfile <<EOL
FROM docker.io/library/golang:1.20.1-bullseye
RUN curl -sSL https://dl.cloudsmith.io/public/caddy/xcaddy/gpg.key | gpg --dearmor > /usr/share/keyrings/xcaddy.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/xcaddy.gpg] https://dl.cloudsmith.io/public/caddy/xcaddy/deb/debian any-version main" > /etc/apt/sources.list.d/xcaddy.list
RUN apt-get update -y
RUN apt-get install -y xcaddy
RUN xcaddy build --with github.com/caddy-dns/gandi
EOL
    podman build -t xcaddy -f /tmp/xcaddy
    container_id=$(podman create xcaddy)
    podman cp $container_id:/go/caddy /usr/local/bin/caddy
    podman rm -v $container_id

    dpkg-divert --divert /usr/bin/caddy.default --rename /usr/bin/caddy
    update-alternatives --install /usr/bin/caddy caddy /usr/bin/caddy.default 10
    update-alternatives --install /usr/bin/caddy caddy /usr/local/bin/caddy 50
fi

systemctl restart caddy
