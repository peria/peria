package login

import (
	"context"

	"codeberg.org/peria/cli/pkg/api"
	"codeberg.org/peria/cli/pkg/client"
	"codeberg.org/peria/cli/pkg/config"
)

func Login(config config.ClientConfig, token string) error {
	config.Token = token
	client := client.PeriaClient{}
	err := client.Start(config)
	if err != nil {
		return err
	}
	_, err = client.RpcClient.Ping(context.Background(), &api.Empty{})
	if err != nil {
		return err
	}
	err = config.Save()
	if err != nil {
		return err
	}
	client.Log("successfully logged in to %s\n", client.Config.ApiUrl)
	return nil
}
