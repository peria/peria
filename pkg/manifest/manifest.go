package manifest

import (
	"fmt"
	"os"
	"path/filepath"

	"codeberg.org/peria/cli/pkg/api"
	"codeberg.org/peria/cli/pkg/client"
	"gopkg.in/yaml.v2"
)

func GuessManifest(client client.PeriaClient) (*api.App, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	yamlFile := filepath.Join(wd, "peria.yaml")
	app, err := LoadFromFile(yamlFile)
	if err == nil {
		return app, nil
	}
	client.Log("no peria manifest found at %s... generating one\n", yamlFile)
	guessedName, err := guessName(wd)
	if err != nil {
		return nil, err
	}
	guessedStack, err := guessStack(wd)
	if err != nil {
		return nil, err
	}
	guessedServicePort, err := guessServicePort(wd)
	if err != nil {
		return nil, err
	}
	app = &api.App{
		Name:        guessedName,
		Stack:       guessedStack,
		ServicePort: guessedServicePort,
	}
	client.Log("generated manifest: %+v\n", app)
	return app, nil
}

func LoadFromFile(file string) (*api.App, error) {
	app := &api.App{}

	fileContents, err := os.ReadFile(file)
	if err != nil {
		return app, err
	}

	err = yaml.Unmarshal(fileContents, &app)
	if err != nil {
		return app, err
	}
	return app, nil
}

func guessName(wd string) (string, error) {
	dirName := filepath.Base(wd)
	if dirName == string(filepath.Separator) || dirName == "." {
		return "", fmt.Errorf("could not guess an application name from the directory: %s", wd)
	}
	return dirName, nil
}

func guessStack(wd string) (*api.Stack, error) {
	dockerfile := filepath.Join(wd, "Dockerfile")
	if _, err := os.Stat(dockerfile); os.IsNotExist(err) {
		return nil, fmt.Errorf("could not guess the application stack at the directory: %s", wd)
	}
	return &api.Stack{Type: "docker"}, nil
}

func guessServicePort(wd string) (uint32, error) {
	return 80, nil
}
