package manifest

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLoadHello(t *testing.T) {
	app, err := LoadFromFile("../../examples/hello_explicit/peria.yaml")
	require.Nil(t, err)
	require.NotNil(t, app.Stack)
	require.Equal(t, "docker", app.Stack.Type)
}

func TestLoadNonExisting(t *testing.T) {
	_, err := LoadFromFile("peria.yaml")
	require.NotNil(t, err)
	require.True(t, os.IsNotExist(err))
}
