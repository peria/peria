package podman

import (
	"fmt"
	"os"
	"os/exec"

	"codeberg.org/peria/cli/pkg/api"
)

type Podman struct {
	ServingFqdn string
}

func (p *Podman) BuildApp(app *api.App, workspace *api.Workspace) error {
	cmd := exec.Command("sudo", "podman", "build", "-t", app.Id, ".")
	cmd.Dir = workspace.Directory
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func (p *Podman) ScheduleApp(app *api.App) error {
	app.Hostname = fmt.Sprintf("%s.%s", app.Id, p.ServingFqdn)
	err := p.stopApp(app)
	if err != nil {
		return err
	}
	return p.runApp(app)
}

func (p *Podman) stopApp(app *api.App) error {
	cmd := exec.Command("sudo", "podman", "rm", "-f", app.Id)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run() // ignore errors on container stop (might not exist)
	return nil
}

func (p *Podman) runApp(app *api.App) error {
	hostPort := app.NodePort
	containerPort := app.ServicePort
	cmd := exec.Command("sudo", "podman",
		"run",
		"--name",
		app.Id,
		"-p",
		fmt.Sprintf("%d:%d", hostPort, containerPort),
		"-d",
		"--rm",
		app.Id)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
