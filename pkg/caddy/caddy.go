package caddy

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"net/http"

	"codeberg.org/peria/cli/pkg/api"
)

type caddyfileData struct {
	ListenPort      int
	AdminFqdn       string
	ServingFqdn     string
	GandiApiToken   string
	HydraPublicPort int
	Apps            []*api.App
}

const caddyfileTemplate = `
{{ .AdminFqdn }} {
    tls {
        dns gandi {{ .GandiApiToken }}
    }
    file_server {
        root /home/peria/web
    }
}

*.{{ .AdminFqdn }} {
    tls {
        dns gandi {{ .GandiApiToken }}
    }

    @auth host auth.{{ .AdminFqdn }}
    handle @auth {
        reverse_proxy http://localhost:{{ .HydraPublicPort }}
    }

    @api host api.{{ .AdminFqdn }}
    handle @api {
        reverse_proxy h2c://localhost:{{ .ListenPort }}
    }
}

*.{{ .ServingFqdn }} {
    tls {
        dns gandi {{ .GandiApiToken }}
    }
    {{ range .Apps }}
	@{{ .Id }} host {{ .Id }}.{{ $.ServingFqdn }}
    handle @{{ .Id }} {
        reverse_proxy localhost:{{ .NodePort }}
    }{{ end }}
}`

type Options struct {
	AdminFqdn       string
	CaddyUrl        string
	GandiApiToken   string
	HydraPublicPort int
	ListenPort      int
	ServingFqdn     string
}

type Caddy struct {
	options Options
	apps    map[string]*api.App
}

func NewFromOptions(options Options) *Caddy {
	return &Caddy{
		options: options,
		apps:    make(map[string]*api.App),
	}
}

func (c *Caddy) PublishApp(app *api.App) error {
	c.apps[app.Id] = app
	return c.reloadConfig()
}

func (c *Caddy) reloadConfig() error {
	apps := []*api.App{}
	for _, app := range c.apps {
		apps = append(apps, app)
	}

	templateInput := caddyfileData{
		ListenPort:      c.options.ListenPort,
		AdminFqdn:       c.options.AdminFqdn,
		ServingFqdn:     c.options.ServingFqdn,
		GandiApiToken:   c.options.GandiApiToken,
		HydraPublicPort: c.options.HydraPublicPort,
		Apps:            apps,
	}

	templateRender, err := template.New("caddyfile").Parse(caddyfileTemplate)
	if err != nil {
		return err
	}
	var caddyfile bytes.Buffer
	err = templateRender.Execute(&caddyfile, templateInput)
	if err != nil {
		return err
	}

	request, err := http.NewRequest(
		http.MethodPost,
		fmt.Sprintf("%s/load", c.options.CaddyUrl),
		bytes.NewBuffer(caddyfile.Bytes()))
	if err != nil {
		return err
	}
	request.Header.Set("Content-Type", "text/caddyfile")
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	if response.StatusCode > 299 {
		responseBytes, err := io.ReadAll(response.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("error updating reverse proxy: %s", string(responseBytes))
	}
	return nil
}
