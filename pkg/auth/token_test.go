package auth

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetClientCredentialsFromToken(t *testing.T) {
	id, secret, err := GetClientCredentialsFromToken("Ym9iYnk6aHVudGVyMg==")
	require.Nil(t, err)
	require.Equal(t, "bobby", id)
	require.Equal(t, "hunter2", secret)
}

func TestGetTokenFromCredentials(t *testing.T) {
	token, err := GetTokenFromCredentials("bobby", "hunter2")
	require.Nil(t, err)
	require.Equal(t, "Ym9iYnk6aHVudGVyMg==", token)
}

func TestReversable(t *testing.T) {
	const expectedToken = "OTI1M2FlYjQtZDFlZi00Y2I4LWFjMGMtYWY3YjMxMzZlOTJmOlNPfm9NS1B+SzZSWXJUaHZudzZyTkRWd3Yz"
	const expectedClientId = "9253aeb4-d1ef-4cb8-ac0c-af7b3136e92f"
	const expectedClientSecret = "SO~oMKP~K6RYrThvnw6rNDVwv3"
	id, secret, err := GetClientCredentialsFromToken(expectedToken)
	require.Nil(t, err)
	require.EqualValues(t, expectedClientId, id)
	require.EqualValues(t, expectedClientSecret, secret)
	token, err := GetTokenFromCredentials(id, secret)
	require.Nil(t, err)
	require.Equal(t, expectedToken, token)
	finalId, finalSecret, err := GetClientCredentialsFromToken(token)
	require.Nil(t, err)
	require.EqualValues(t, expectedClientId, finalId)
	require.EqualValues(t, expectedClientSecret, finalSecret)
}
