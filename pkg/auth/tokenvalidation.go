package auth

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	errMissingMetadata = status.Errorf(codes.InvalidArgument, "missing metadata")
	errInvalidToken    = status.Errorf(codes.Unauthenticated, "invalid token")
)

type TokenValidator struct {
	OidcUrl string
}

func (s *TokenValidator) Validate(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return errMissingMetadata
	}
	authorizationHeader := md["authorization"]
	if len(authorizationHeader) < 1 {
		return errInvalidToken
	}
	stringToken := strings.TrimPrefix(authorizationHeader[0], "Bearer ")
	_, err := jwt.Parse(stringToken, s.getKey)
	if err != nil {
		return err
	}
	return nil
}

func (s *TokenValidator) getKey(token *jwt.Token) (interface{}, error) {
	ctx := context.Background()
	// TODO: cache response so we don't have to make a request every time
	// we want to verify a JWT
	set, err := jwk.Fetch(ctx, fmt.Sprintf("%s/.well-known/jwks.json", s.OidcUrl))
	if err != nil {
		return nil, err
	}

	keyID, ok := token.Header["kid"].(string)
	if !ok {
		return nil, errors.New("expecting JWT header to have string kid")
	}

	for i := 0; i < set.Len(); i++ {
		key, _ := set.Key(i)
		if key.KeyID() == keyID {
			pkey := rsa.PublicKey{}
			err = key.Raw(&pkey)
			if err != nil {
				return nil, err
			}
			return &pkey, nil
		}
	}

	return nil, fmt.Errorf("unable to find key %q", keyID)
}
