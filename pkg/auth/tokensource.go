package auth

import (
	"context"
	"time"

	"codeberg.org/peria/cli/pkg/config"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"google.golang.org/grpc/credentials/oauth"
)

type PeriaTokenSource struct {
	oauth.TokenSource
	Config config.ClientConfig
}

func (s PeriaTokenSource) Token() (*oauth2.Token, error) {
	clientId, clientSecret, err := GetClientCredentialsFromToken(s.Config.Token)
	if err != nil {
		return nil, err
	}
	authConfig := clientcredentials.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		TokenURL:     s.Config.TokenUrl,
		Scopes:       []string{"oidc"},
		AuthStyle:    oauth2.AuthStyleInParams,
	}
	authCtx, authCancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer authCancel()
	token, err := authConfig.Token(authCtx)
	if err != nil {
		return nil, err
	}
	return token, nil
}
