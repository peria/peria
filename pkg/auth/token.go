package auth

import (
	"encoding/base64"
	"fmt"
	"strings"
)

func GetClientCredentialsFromToken(token string) (string, string, error) {
	decoded, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return "", "", err
	}
	decodedString := string(decoded)
	parts := strings.Split(decodedString, ":")
	if len(parts) != 2 {
		return "", "", fmt.Errorf("authentication token with invalid format")
	}
	clientId := parts[0]
	if len(clientId) < 1 {
		return "", "", fmt.Errorf("authentication token with invalid format")
	}
	clientSecret := parts[1]
	if len(clientSecret) < 1 {
		return "", "", fmt.Errorf("authentication token with invalid format")
	}
	return clientId, clientSecret, nil
}

func GetTokenFromCredentials(clientId string, clientSecret string) (string, error) {
	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", clientId, clientSecret))), nil
}
