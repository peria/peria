package db

const schema = `
create extension if not exists "uuid-ossp";

create table if not exists accounts();
alter table accounts add column if not exists idx serial unique;
alter table accounts add column if not exists id uuid primary key default uuid_generate_v4();
alter table accounts add column if not exists created_stamp timestamp not null default current_timestamp;
alter table accounts add column if not exists name text not null;

create table if not exists tokens();
alter table tokens add column if not exists idx serial unique;
alter table tokens add column if not exists id uuid primary key default uuid_generate_v4();
alter table tokens add column if not exists created_stamp timestamp not null default current_timestamp;
alter table tokens add column if not exists name text not null;
alter table tokens add column if not exists token text not null;
alter table tokens add column if not exists account_id uuid not null references accounts(id);

create table if not exists apps();
alter table apps add column if not exists idx serial unique;
alter table apps add column if not exists id uuid primary key default uuid_generate_v4();
alter table apps add column if not exists created_stamp timestamp not null default current_timestamp;
alter table apps add column if not exists name text not null;
alter table apps add column if not exists account_id uuid not null references accounts(id);

create table if not exists workspaces();
alter table workspaces add column if not exists idx serial unique;
alter table workspaces add column if not exists id uuid primary key default uuid_generate_v4();
alter table workspaces add column if not exists created_stamp timestamp not null default current_timestamp;
alter table workspaces add column if not exists name text not null;
alter table workspaces add column if not exists app_id uuid not null references apps(id);
alter table workspaces add column if not exists account_id uuid not null references accounts(id);
`
