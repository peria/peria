package db

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq"
)

type PeriaDb struct {
	db *sql.DB
}

func (d *PeriaDb) Start(dsn string) error {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return err
	}
	d.db = db
	err = d.Migrate()
	if err != nil {
		return err
	}
	return nil
}

func (d *PeriaDb) Stop() error {
	if d.db != nil {
		return d.db.Close()
	}
	return nil
}

func (d *PeriaDb) Migrate() error {
	ctx := context.Background()
	connection, err := d.db.Conn(ctx)
	if err != nil {
		return err
	}
	defer connection.Close()
	_, err = connection.ExecContext(ctx, schema)
	if err != nil {
		return err
	}
	return nil
}

type Account struct {
	Id   string
	Name string
}

func (d *PeriaDb) UpsertRootAccount() (Account, error) {
	ctx := context.Background()
	account := Account{}
	connection, err := d.db.Conn(ctx)
	if err != nil {
		return account, err
	}
	defer connection.Close()
	insertQuery := `insert into accounts(idx, name) values (0, 'root')
	on conflict(idx) do update set name = EXCLUDED.name
	returning id, name`
	row := d.db.QueryRowContext(ctx, insertQuery)
	err = row.Scan(&account.Id, &account.Name)
	if err != nil {
		return account, err
	}
	return account, nil
}

func (d *PeriaDb) HasTokens(account Account) (bool, error) {
	getAnyTokenId := `select id from tokens where account_id=$1 limit 1`
	ctx := context.Background()
	connection, err := d.db.Conn(ctx)
	if err != nil {
		return false, err
	}
	defer connection.Close()
	statement, err := d.db.PrepareContext(ctx, getAnyTokenId)
	if err != nil {
		return false, err
	}
	defer statement.Close()
	rows, err := statement.QueryContext(ctx, account.Id)
	if err != nil {
		return false, err
	}
	if !rows.Next() {
		return false, nil
	}
	return true, nil
}

type Token struct {
	Id        string
	Name      string
	Token     string
	AccountId string
}

func (d *PeriaDb) CreateToken(token Token) (Token, error) {
	insertToken := `insert into tokens(name, token, account_id) values($1, $2, $3)
	returning id, name, token, account_id`
	ctx := context.Background()
	connection, err := d.db.Conn(ctx)
	if err != nil {
		return token, err
	}
	defer connection.Close()
	statement, err := d.db.PrepareContext(ctx, insertToken)
	if err != nil {
		return token, err
	}
	defer statement.Close()
	row := statement.QueryRowContext(ctx, token.Name, token.Token, token.AccountId)
	err = row.Scan(&token.Id, &token.Name, &token.Token, &token.AccountId)
	if err != nil {
		return token, err
	}
	return token, nil
}
