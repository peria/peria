package cli

import (
	"codeberg.org/peria/cli/pkg/client"
	"codeberg.org/peria/cli/pkg/config"
	"codeberg.org/peria/cli/pkg/login"
	"codeberg.org/peria/cli/pkg/publish"
	"github.com/spf13/cobra"
)

type PeriaCli struct {
	config  config.ClientConfig
	client  client.PeriaClient
	command cobra.Command
}

func (c *PeriaCli) Start(config config.ClientConfig) error {
	c.config = config
	c.client = client.PeriaClient{}
	err := c.client.Start(config)
	if err != nil {
		return err
	}
	c.createCommands()
	err = c.command.Execute()
	if err != nil {
		return err
	}
	return nil
}

func (c *PeriaCli) Stop() error {
	return c.client.Stop()
}

func (c *PeriaCli) createCommands() {
	c.command = cobra.Command{
		Use: "peria",
	}
	c.command.AddCommand(
		&cobra.Command{
			Use:     "login <token>",
			Short:   "logs in to Peria",
			Long:    "Stores authentication token at ${HOME}/.peria",
			Example: "peria login <token>",
			Args:    cobra.ExactArgs(1),
			RunE: func(cmd *cobra.Command, args []string) error {
				return login.Login(c.config, args[0])
			},
		},
		&cobra.Command{
			Use:   "publish",
			Short: "Publishes an application",
			RunE: func(cmd *cobra.Command, args []string) error {
				return publish.Publish(c.client)
			},
		})
}
