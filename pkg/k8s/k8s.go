package k8s

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"codeberg.org/peria/cli/pkg/api"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type K8s struct {
}

func (k *K8s) Start() error {
	clientset, err := k.auth()
	if err != nil {
		return err
	}
	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return err
	}
	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))
	return nil
}

func (k *K8s) auth() (*kubernetes.Clientset, error) {
	config, err := rest.InClusterConfig()
	if err == nil {
		return kubernetes.NewForConfig(config)
	}
	log.Printf("could not authenticate from within the k8s cluster, attempting to read kube config")
	homedir, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}
	kubeconfig := filepath.Join(homedir, ".kube", "config")
	config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func (k *K8s) ScheduleApp(app *api.App) error {
	return fmt.Errorf("k8s support not implemented")
}
