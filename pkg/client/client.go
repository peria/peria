package client

import (
	"context"
	"crypto/x509"
	"log"
	"os"

	"codeberg.org/peria/cli/pkg/api"
	"codeberg.org/peria/cli/pkg/auth"
	"codeberg.org/peria/cli/pkg/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/oauth"
)

type PeriaClient struct {
	RpcClient   api.PeriaClient
	Config      config.ClientConfig
	Workdir     string
	connection  *grpc.ClientConn
	tokenSource auth.PeriaTokenSource
}

func (c *PeriaClient) Start(config config.ClientConfig) error {
	workdir, err := os.Getwd()
	if err != nil {
		return err
	}
	c.Workdir = workdir

	c.tokenSource = auth.PeriaTokenSource{Config: config}

	pool, err := x509.SystemCertPool()
	if err != nil {
		return err
	}
	creds := credentials.NewClientTLSFromCert(pool, "")
	opts := []grpc.DialOption{
		//grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithTransportCredentials(creds),
		grpc.WithUnaryInterceptor(c.unaryInterceptor),
		grpc.WithStreamInterceptor(c.streamInterceptor),
	}
	connection, err := grpc.Dial(config.ApiUrl, opts...)
	if err != nil {
		return err
	}
	c.connection = connection
	c.RpcClient = api.NewPeriaClient(connection)
	return nil
}

func (c *PeriaClient) Stop() error {
	if c.connection != nil {
		return c.connection.Close()
	}
	return nil
}

func (c *PeriaClient) Log(format string, v ...any) {
	log.Printf(format, v...)
}

func (c *PeriaClient) unaryInterceptor(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	opts = append(opts, grpc.PerRPCCredentials(oauth.TokenSource{TokenSource: c.tokenSource}))
	return invoker(ctx, method, req, reply, cc, opts...)
}

func (c *PeriaClient) streamInterceptor(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	opts = append(opts, grpc.PerRPCCredentials(oauth.TokenSource{TokenSource: c.tokenSource}))
	return streamer(ctx, desc, cc, method, opts...)
}
