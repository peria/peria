package config

import (
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

type ClientConfig struct {
	ApiUrl    string `yaml:"api_url,omitempty" default:"api.peria.pt:443" constraints:"mandatory"`
	TokenUrl  string `yaml:"token_url,omitempty" default:"https://auth.peria.pt/oauth2/token" constraints:"mandatory"`
	Token     string `yaml:"token,omitempty" constraints:"mandatory"`
	CaPemFile string `yaml:"ca_pem_file,omitempty"`
}

func (c *ClientConfig) Save() error {
	configFileContents, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	periaConfigDir := filepath.Join(homeDir, ".peria")
	err = os.MkdirAll(periaConfigDir, 0700)
	if err != nil {
		return err
	}
	configFilePath := filepath.Join(homeDir, ".peria", "config.yaml")
	err = os.WriteFile(configFilePath, configFileContents, 0600)
	if err != nil {
		return err
	}
	return nil
}
