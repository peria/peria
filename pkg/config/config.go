package config

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

var stringType = reflect.TypeOf("")
var intType = reflect.TypeOf(int(0))

func Load(c interface{}) error {
	var err error
	err = loadDefaults(c)
	if err != nil {
		return nil
	}
	err = loadFromDefaultFile(c)
	if err != nil {
		return err
	}
	err = loadFromEnv(c)
	if err != nil {
		return err
	}
	return assertConstraints(c)
}

func loadDefaults(to interface{}) error {
	configType := reflect.TypeOf(to).Elem()
	configValue := reflect.ValueOf(to).Elem()
	fields := reflect.VisibleFields(configType)
	for i, field := range fields {
		tag, isSet := field.Tag.Lookup("default")
		if isSet {
			defaultValue := tag
			fieldValue := configValue.Field(i)
			if fieldValue.CanSet() {
				if field.Type == stringType {
					fieldValue.SetString(defaultValue)
				} else if field.Type == intType {
					intValue, err := strconv.Atoi(defaultValue)
					if err != nil {
						return fmt.Errorf("could not parse default value '%s' for field '%s' as integer", defaultValue, field.Name)
					} else {
						fieldValue.SetInt(int64(intValue))
					}
				} else {
					return fmt.Errorf("default values not supported for type '%s' at field %s", field.Type.Name(), field.Name)
				}
			}
		}
	}
	return nil
}

func assertConstraints(to interface{}) error {
	configType := reflect.TypeOf(to).Elem()
	configValue := reflect.ValueOf(to).Elem()
	fields := reflect.VisibleFields(configType)
	for i, field := range fields {
		tag, isSet := field.Tag.Lookup("constraints")
		if isSet {
			for _, constraint := range strings.Split(tag, ",") {
				if constraint == "" {
					continue
				}
				if constraint == "mandatory" {
					if field.Type == stringType {
						fieldValue := configValue.Field(i)
						if fieldValue.String() == "" {
							return fmt.Errorf("%s cannot be empty", field.Type.Name())
						}
					} else {
						return fmt.Errorf("unsupported contstraint (%s) for field type %s", constraint, field.Type.Name())
					}
				} else {
					return fmt.Errorf("unsupported contstraint (%s) for field %s", constraint, field.Name)
				}
			}
		}
	}
	return nil
}

func loadFromDefaultFile(to interface{}) error {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	configFilePath := filepath.Join(homeDir, ".peria", "config.yaml")
	configFileContents, err := os.ReadFile(configFilePath)
	if os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return err
	}
	return yaml.Unmarshal(configFileContents, to)
}

func loadFromEnv(to interface{}) error {
	configType := reflect.TypeOf(to).Elem()
	configValue := reflect.ValueOf(to).Elem()
	fields := reflect.VisibleFields(configType)
	for i, field := range fields {
		yamlTag, isSet := field.Tag.Lookup("yaml")
		if isSet {
			yamlFieldName, _, _ := strings.Cut(yamlTag, ",")
			envKey := strings.ToUpper(fmt.Sprintf("PERIA_%s", yamlFieldName))
			envStringValue, isEnvSet := os.LookupEnv(envKey)
			fieldValue := configValue.Field(i)
			if isEnvSet && fieldValue.CanSet() {
				if field.Type == stringType {
					fieldValue.SetString(envStringValue)
				} else if field.Type == intType {
					intValue, err := strconv.Atoi(envStringValue)
					if err != nil {
						return fmt.Errorf("could not parse environment variable '%s' as integer\n", envKey)
					} else {
						fieldValue.SetInt(int64(intValue))
					}
				} else {
					return fmt.Errorf("type '%s' can't be loaded from environment variable (%s)\n", field.Type.Name(), envKey)
				}
			}
		}
	}
	return nil
}
