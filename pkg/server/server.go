package server

import (
	"context"
	"fmt"
	"log"
	"net"

	"codeberg.org/peria/cli/pkg/api"
	"codeberg.org/peria/cli/pkg/auth"
	"codeberg.org/peria/cli/pkg/caddy"
	"codeberg.org/peria/cli/pkg/db"
	"codeberg.org/peria/cli/pkg/hydra"
	"codeberg.org/peria/cli/pkg/k8s"
	"codeberg.org/peria/cli/pkg/lifecycle"
	"codeberg.org/peria/cli/pkg/podman"
	"codeberg.org/peria/cli/pkg/publish"
	"google.golang.org/grpc"
)

type PeriaServer struct {
	api.UnimplementedPeriaServer
	config         Config
	db             db.PeriaDb
	caddy          *caddy.Caddy
	podman         *podman.Podman
	k8s            *k8s.K8s
	tokenValidator *auth.TokenValidator
	rpcServer      *grpc.Server
}

func (s *PeriaServer) Start(config Config) error {
	var err error
	s.config = config

	s.caddy = caddy.NewFromOptions(caddy.Options{
		AdminFqdn:       config.AdminFqdn,
		CaddyUrl:        config.CaddyUrl,
		GandiApiToken:   config.GandiApiToken,
		HydraPublicPort: config.HydraPublicPort,
		ListenPort:      config.ListenPort,
		ServingFqdn:     config.ServingFqdn,
	})

	s.podman = &podman.Podman{
		ServingFqdn: config.ServingFqdn,
	}

	if config.Runtime == "k8s" {
		s.k8s = &k8s.K8s{}
		err := s.k8s.Start()
		if err != nil {
			return err
		}
	}

	err = s.db.Start(config.Dsn)
	if err != nil {
		return err
	}

	err = s.EnsureRootAccount()
	if err != nil {
		return err
	}

	s.tokenValidator = &auth.TokenValidator{OidcUrl: config.OidcUrl}

	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(s.authCall),
		grpc.StreamInterceptor(s.authStream),
	}
	s.rpcServer = grpc.NewServer(opts...)
	api.RegisterPeriaServer(s.rpcServer, s)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", config.ListenPort))
	if err != nil {
		return err
	}
	log.Printf("server listening at %v", lis.Addr())
	return s.rpcServer.Serve(lis)
}

func (s *PeriaServer) Stop() error {
	if s.rpcServer != nil {
		s.rpcServer.Stop()
	}
	return nil
}

func (s *PeriaServer) authCall(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	err := s.tokenValidator.Validate(ctx)
	if err != nil {
		return nil, err
	}
	return handler(ctx, req)
}

func (s *PeriaServer) authStream(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	err := s.tokenValidator.Validate(ss.Context())
	if err != nil {
		return err
	}
	return handler(srv, ss)
}

func (s *PeriaServer) EnsureRootAccount() error {
	account, err := s.db.UpsertRootAccount()
	if err != nil {
		return err
	}
	hasTokens, err := s.db.HasTokens(account)
	if err != nil {
		return err
	}
	if !hasTokens {
		oauth2Client, err := hydra.CreateClient(hydra.Options{AdminUrl: s.config.HydraAdminUrl}, account.Id)
		if err != nil {
			return err
		}
		token, err := auth.GetTokenFromCredentials(oauth2Client.ClientId, oauth2Client.ClientSecret)
		if err != nil {
			return err
		}
		_, err = s.db.CreateToken(db.Token{
			Name:      "default",
			Token:     token,
			AccountId: account.Id,
		})
		if err != nil {
			return err
		}
		log.Printf("root account '%s' created with token '%s'\n", account.Id, token)
	}
	return nil
}

func (s *PeriaServer) Ping(context.Context, *api.Empty) (*api.Empty, error) {
	return &api.Empty{}, nil
}

func (s *PeriaServer) PublishFromArchive(request api.Peria_PublishFromArchiveServer) error {
	var scheduler lifecycle.AppScheduler
	scheduler = s.podman
	if s.config.Runtime == "k8s" {
		scheduler = s.k8s
	}
	return publish.PublishFromArchive(request, s.podman, scheduler, s.caddy)
}
