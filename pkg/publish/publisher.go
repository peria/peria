package publish

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"sync"

	"codeberg.org/peria/cli/pkg/api"
	"codeberg.org/peria/cli/pkg/filesystem"
	"codeberg.org/peria/cli/pkg/lifecycle"
	"codeberg.org/peria/cli/pkg/workspaces"
)

func PublishFromArchive(stream api.Peria_PublishFromArchiveServer,
	appBuilder lifecycle.AppBuilder,
	appScheduler lifecycle.AppScheduler,
	appPublisher lifecycle.AppPublisher) error {
	statusReport := make(chan *api.PublishStatus)

	var wg sync.WaitGroup

	go func() {
		wg.Add(1)
		for {
			report, ok := <-statusReport
			if !ok {
				break
			}
			err := stream.Send(report)
			if err != nil {
				log.Printf("error sending publish state (ignoring): %s\n", err)
			}
		}
		wg.Done()
	}()

	publishRequest, err := stream.Recv()
	if err != nil {
		return err
	}
	app := publishRequest.App
	app.Id = app.Name
	app.ServicePort = 80
	foundNodePort, err := FindFreeNodePort()
	if err != nil {
		return err
	}
	app.NodePort = uint32(foundNodePort)
	workspace := &api.Workspace{
		Id:  app.Id,
		App: app,
	}
	workspaceDirectory, err := workspaces.GetWorkspaceDirectory(workspace)
	if err != nil {
		return err
	}
	workspace.Directory = workspaceDirectory
	if _, err := os.Stat(workspaceDirectory); errors.Is(err, os.ErrNotExist) {
		err = os.Mkdir(workspaceDirectory, os.FileMode(0700))
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}
	downloader := &downloader{
		statusReport: statusReport,
		app:          app,
		hasPrevious:  true,
		stream:       &stream,
		previous:     publishRequest.Archive.Raw,
	}
	statusReport <- &api.PublishStatus{
		App:      app,
		Progress: 0.1,
		Message:  fmt.Sprintf("unpacking assets for application '%s'", app.Name),
	}
	err = filesystem.Uncompress(downloader, workspace.Directory)
	if err != nil {
		return err
	}
	statusReport <- &api.PublishStatus{
		App:      app,
		Progress: 0.5,
		Message:  fmt.Sprintf("building application '%s'", app.Name),
	}
	err = appBuilder.BuildApp(app, workspace)
	if err != nil {
		return err
	}
	statusReport <- &api.PublishStatus{
		App:      app,
		Progress: 0.6,
		Message:  fmt.Sprintf("deploying application '%s'", app.Name),
	}
	err = appScheduler.ScheduleApp(app)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	statusReport <- &api.PublishStatus{
		App:      app,
		Progress: 0.8,
		Message:  fmt.Sprintf("publishing application '%s'", app.Name),
	}
	err = appPublisher.PublishApp(app)
	if err != nil {
		return err
	}
	statusReport <- &api.PublishStatus{
		App:      app,
		Progress: 1,
		Message:  fmt.Sprintf("application '%s' published and runnning at https://%s", app.Name, app.Hostname),
	}
	close(statusReport)
	wg.Wait()
	return nil
}

func FindFreeNodePort() (int, error) {
	if a, err := net.ResolveTCPAddr("tcp", "127.0.0.1:0"); err == nil {
		var l *net.TCPListener
		if l, err = net.ListenTCP("tcp", a); err == nil {
			defer l.Close()
			return l.Addr().(*net.TCPAddr).Port, nil
		}
	}
	return 0, fmt.Errorf("network exhausted: no more free ports")
}
