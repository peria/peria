package publish

import (
	"context"
	"io"

	"codeberg.org/peria/cli/pkg/client"
	"codeberg.org/peria/cli/pkg/filesystem"
	"codeberg.org/peria/cli/pkg/manifest"
)

func Publish(client client.PeriaClient) error {
	app, err := manifest.GuessManifest(client)
	if err != nil {
		return err
	}

	client.Log("publishing application '%s'\n", app.Name)

	stream, err := client.RpcClient.PublishFromArchive(context.Background())
	if err != nil {
		return err
	}
	waitc := make(chan struct{})

	go func() {
		for {
			publishResponse, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				client.Log("error reading publish response %+v\n", err)
				close(waitc)
				return
			}
			client.Log("[%.0f%s] publish application %s: %s\n",
				publishResponse.Progress*100, "%", app.Name, publishResponse.Message)
		}
	}()

	writer := uploader{
		app:    app,
		stream: stream,
	}
	err = filesystem.Compress(client.Workdir, &writer)
	if err != nil {
		return err
	}
	stream.CloseSend()
	<-waitc
	return nil
}
