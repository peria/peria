package publish

import (
	"fmt"
	"io"
	"time"

	"codeberg.org/peria/cli/pkg/api"
)

type downloader struct {
	statusReport chan *api.PublishStatus
	stream       *api.Peria_PublishFromArchiveServer
	app          *api.App
	hasPrevious  bool
	previous     []byte
	total        int
	lastReport   time.Time
}

func (d *downloader) Read(p []byte) (n int, err error) {
	if d.hasPrevious {
		d.hasPrevious = false
		d.lastReport = time.Now()
		d.total += len(d.previous)
		return copy(p, d.previous), nil
	}
	publishRequest, err := (*d.stream).Recv()
	if err == io.EOF {
		return 0, err
	}
	if err != nil {
		return 0, err
	}
	d.total += len(publishRequest.Archive.Raw)
	if time.Since(d.lastReport) > time.Second {
		d.lastReport = time.Now()
		d.statusReport <- &api.PublishStatus{
			App:      d.app,
			Progress: 0.1,
			Message:  fmt.Sprintf("%d bytes uploaded assets for application '%s'", d.total, d.app.Name),
		}
	}
	return copy(p, publishRequest.Archive.Raw), nil
}
