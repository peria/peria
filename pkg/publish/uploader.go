package publish

import "codeberg.org/peria/cli/pkg/api"

type uploader struct {
	app    *api.App
	stream api.Peria_PublishFromArchiveClient
}

func (u *uploader) Write(p []byte) (n int, err error) {
	publishRequest := &api.PublishRequest{
		App: u.app,
		Archive: &api.Archive{
			Raw: p,
		},
	}
	err = u.stream.Send(publishRequest)
	if err != nil {
		return 0, err
	}
	return len(p), nil
}
