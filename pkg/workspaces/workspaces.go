package workspaces

import (
	"os"
	"path/filepath"

	"codeberg.org/peria/cli/pkg/api"
)

func GetWorkspaceDirectory(workspace *api.Workspace) (string, error) {
	return filepath.Join(os.TempDir(), workspace.Id), nil
}
