package hydra

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Options struct {
	AdminUrl string
}

type Oauth2Client struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

func CreateClient(options Options, accountId string) (Oauth2Client, error) {
	oauth2Client := Oauth2Client{}
	createClient := []byte(fmt.Sprintf(`{
		"grant_types": ["client_credentials"],
		"response_types": ["code"],
		"scope": "oidc",
		"client_secret_expires_at": 0,
		"subject_type": "public",
		"token_endpoint_auth_method": "client_secret_post",
		"metadata": {"account_id": "%s"}
	  }`, accountId))
	request, err := http.NewRequest(
		http.MethodPost,
		fmt.Sprintf("%s/admin/clients", options.AdminUrl),
		bytes.NewBuffer(createClient))
	if err != nil {
		return oauth2Client, err
	}
	request.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return oauth2Client, err
	}
	defer response.Body.Close()
	responseBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return oauth2Client, err
	}
	if response.StatusCode > 299 {
		return oauth2Client, fmt.Errorf("error (%d) creating oauth2 client: %s", response.StatusCode, string(responseBytes))
	}
	err = json.Unmarshal(responseBytes, &oauth2Client)
	if err != nil {
		return oauth2Client, err
	}
	return oauth2Client, nil
}
