package main

import (
	"log"

	"codeberg.org/peria/cli/pkg/config"
	"codeberg.org/peria/cli/pkg/server"
)

func main() {
	var err error
	serverConfig := server.Config{}
	err = config.Load(&serverConfig)
	if err != nil {
		log.Fatal(err)
	}
	periaServer := server.PeriaServer{}
	err = periaServer.Start(serverConfig)
	if err != nil {
		log.Fatal(err)
	}
}
