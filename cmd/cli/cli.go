package main

import (
	"log"

	"codeberg.org/peria/cli/pkg/cli"
	"codeberg.org/peria/cli/pkg/config"
)

func main() {
	var err error
	clientConfig := config.ClientConfig{}
	err = config.Load(&clientConfig)
	if err != nil {
		log.Fatal(err)
	}
	periaCli := cli.PeriaCli{}
	err = periaCli.Start(clientConfig)
	if err != nil {
		log.Fatal(err)
	}
}
