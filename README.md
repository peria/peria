# Peria
A serverless stack  
> Shaving yaks so you don't have to

## Local k8s dev
Requires:
- [golang](https://go.dev/doc/install)
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/).
- [tilt](https://tilt.dev/)

Add `api.peria.pt` to `/etc/hosts` 🤡

Run `make kind` to create a development `k8s` cluster.

Run `tilt up` to get Peria up and running.

Run `make hello` to publish a sample 'hello' app

## Remote podman dev
To work with single-server production environment:
- An SSH key pair has to be setup for `make deploy-*` targets to work

Create a `.env` (git ignored) file in the project root with the contents:
```sh
PERIA_ADMIN_FQDN=peria.tiagosimao.com
PERIA_SERVING_FQDN=app.peria.tiagosimao.com
PERIA_GANDI_API_TOKEN=...
```

After bootstrap (`make cloud-init`), run this in the server:
```
hydra -e http://localhost:4445 create client --grant-type client_credentials --scope oidc --token-endpoint-auth-method client_secret_post
```

Find out what's your authentication token
```
printf "<clientId>:<clientSecret>" | base64
```

Now run `make build-local` and then `peria login <the token>`

All Makefile targets will work from here on

## TODO

### Milestone 1: Trial with working PoC
Maximize DX for a simple workload, a stateless containerized application with small storage/memory/cpu footprint.  
A CLI that allows the user to publish applications.  
No git integration, only a folder with an OCI descriptor and maybe a `peria` config file

Trial users only (credentials generated manually)  
Each user can create a limited number applications  
Each user can do a limited number of API calls  
Each application can have a limited workspace size  
Each application can have a limited memory footprint  
Each application can have a limited image footprint  
Each application can have a limited cpu usage  
Each application can have a limited inbound http traffic  
Each application can have a limited outbound http traffic  

```
client config lookup: flags > envs > explicit config file > .peria/config.yaml > ~/.peria/config.yaml > default values > stdin (mandatory only)
add trusted CAs to client config
use k8s as serving runtime
setup local k8s env
test a real stateless workload
run peria server with non-root
test all pkg modules
resource ownership (token maps to N resources, requires DB)
app purge (requires authorization, app DB, app ownership)
test infrastructural regen
streamline cli config (cli toggles > env vars > config file)
assert missing cli config (e.g. missing client id/secret)
cli docs
CI
idempotent publish (requires app registration/DB)
serial publish (same app can't be published in parallel)
fair usage api calls
fair usage workspace CPU, Memory, Storage, IO, Network
fair usage app CPU, Memory, Storage, IO, Network
block app egress traffic to local network
peria api monitoring
trials, user feedback
```

### Milestone 2: Closed beta
```
logo
multi-node support (probably replace backend with k8s)
env/configuration/secrets
windows support
storage, filesystem
monitoring
move to dedicated FQDN
payment/billing
charging/service suspension
horizontal scaling for app scheduling
account registration (invitation)
```

## Dogfooding it
Peria should eventually run on Peria. These are the expected requirements so far:
```
Packed in a single OCI image
Exposes a single TCP port
expects gRPC (HTTP2) traffic
Enforces OIDC, needs a JWKS url, bonus points for auth enforcement done for free
maybe:
    PostgreSQL
    pubsub
    obs stack
    analytics stack
```
